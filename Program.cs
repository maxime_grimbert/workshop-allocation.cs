﻿using System;
using System.IO;
using System.Collections.Generic;

namespace workshop_ventilation
{
    class Program
    {
        static void Main(string[] args)
        {
            // Verbose
            Console.WriteLine("Bonjour ! \n");

            AllFunctions uniqueInstanceOfAllFunctions = new AllFunctions();

            // Debug
            // Console.WriteLine("Debug : Instanciation des fonctions effectuées. \n");

            // Verbose 
            Console.WriteLine("I - Je commence par importer les données du fichier CSV et constituer la liste des participant.e.s aux ateliers. \n");

            uniqueInstanceOfAllFunctions.SetUpAttendees();

            // Verbose
            Console.WriteLine("II - 1 - Ensuite, j'agrége les thèmes de prédilection des répondant.e.s au formulaire et les trie par popularité afin de déterminer les thèmes qui seront exploités par les ateliers. \n");

            uniqueInstanceOfAllFunctions.SetUpThemes();

            // Verbose
            Console.WriteLine("II - 2 - Ensuite, je trie les thèmes de prédilection en fonction de leur fréquence pour déterminer les plus appréciés et les moins appréciés. \n");

            uniqueInstanceOfAllFunctions.SortThemes();

            // Verbose
            Console.WriteLine("III - Ensuite, je créée les atelier sur la base des thèmes de prédilection exprimés les plus appréciés et ceux appréciés moyennement (en excluant donc les moins appréciés). \n");

            uniqueInstanceOfAllFunctions.SetUpWorkshops();

            // Verbose
            Console.WriteLine("IV - Ensuite, je remplis les ateliers avec les participant.e.s en accordant la priorité aux animateur.rice.s (seulement car il faut qu'il y en ait un.e par atelier) puis aux choix exprimés 1, 2 et 3. Les non-répondant.e.s sont réparties aléatoirement à la fin. \n");

            uniqueInstanceOfAllFunctions.FillInWorkshops();

            // Verbose
            uniqueInstanceOfAllFunctions.ListWorkshopsAndMembers();

            // Verbose
            Console.WriteLine("VI - Enfin, j'exporte les résultats dans un nouveau CSV. \n");
            uniqueInstanceOfAllFunctions.OutputCSV();
            // Debug
            // Console.WriteLine("Debug : Génération de outCsv effectuée. \n");

            //Verbose
            Console.WriteLine("Tout ok. Au revoir !");

            // Debug
            // Utile uniquement si le programme se ferme avant de pouvoir consulter les logs et résultats (ce qui est le cas par défaut en release).
            Console.WriteLine("Nb : Appuyer sur n'importe quelle touche fermera le programme.");
            Console.ReadKey();
        }
    }

    class Attendee
    {
        // Un.e participant.e est construit.e ainsi dans le programme (une variable = une colone dans le tableur sauf les thèmes préférés pour lesquels il y a plusieurs colones, une pour chaque thème préféré) :
        public string LastNames;
        public string FirstNames; // peut être composé avec un ou plusieurs traits d'union mais peut aussi être plusieurs prénoms à la suite
        public string[] preferedThemes = new string[3];
        public bool actuallyShowedUp = false;
        public bool animator = false;
        public Workshop assignedWorkshop;
    }
    class Workshop
    {
        // un atelier est construit ainsi
        public Theme theme;
        public int number;
        public List<Attendee> members = new List<Attendee>();
    }
    class Theme
    {
        // un thème est construit ainsi
        public string name;
        public int occurrenceNumber;
        public bool isMostPrefered;
    }
    class AllFunctions
    {
        // I - Pour créer les participant.e.s depuis un document extérieur
        public List<Attendee> allAttendees = new List<Attendee>(); // Celle qu'on va créer maintenant

        public void SetUpAttendees()
        {
            StreamReader inputCSV = new StreamReader(@"input.csv");

            int headerGuard = 0;

            int attendeesCreated = 0;
            while (true)
            {
                string inputCSVLine = inputCSV.ReadLine();
                if (inputCSVLine == null)
                {
                    break;
                }
                else if (headerGuard == 0)
                {
                    headerGuard++;
                }
                else
                {
                    SetUpAttendee(inputCSVLine);
                    attendeesCreated++;
                    // Debug
                    // Console.WriteLine("Débug : J'en suis à {0} travailleureuses crees. Au total, il y a {1} travailleureuses dans la liste.", nombreDeTravailleureusesCreees, toustesLesTravailleureusesEGO.Count);
                }
            }
            // Verbose
            Console.WriteLine("J'ai bien créée les fiches de {0} travailleureuses et donc participant.e.s potentiel.le.s au séminaire. \n", attendeesCreated);
        }
        public void SetUpAttendee(string inputCSVLine)
        {
            Attendee newAttendee = new Attendee();

            String[] parsedInputCSVLine = inputCSVLine.Split(","); // Devrait retourner un array de string qui sont divisé selon les colones du tableur

            // Remplir la nouvelle instance de travailleureuse selon les infos parsées du CSV
            newAttendee.LastNames = parsedInputCSVLine[0];
            newAttendee.FirstNames = parsedInputCSVLine[1]; // peut être composé avec un ou plusieurs traits d'union mais peut aussi être plusieurs prénoms à la suite

            newAttendee.preferedThemes[0] = parsedInputCSVLine[2];
            newAttendee.preferedThemes[1] = parsedInputCSVLine[3];
            newAttendee.preferedThemes[2] = parsedInputCSVLine[4];


            if (parsedInputCSVLine[5] == "Oui")
            {
                newAttendee.animator = true;
            }
            if (parsedInputCSVLine[6] == "Oui")
            {
                newAttendee.actuallyShowedUp = true;
            }

            // Ajouter cette nouvelleau travailleureuse à la liste complète
            allAttendees.Add(newAttendee);
        }

        // II - Pour créer les thèmes qui seront exploités par les atleiers sur la base de ce qui a été exprimé comme choix numéro 1
        public List<Theme> allPreferedThemes = new List<Theme>();
        public void SetUpThemes()
        {
            int attendeeDealtWith = 0;
            // liste les thèmes préférés exprimés et leur nombre d'occurrence
            foreach (Attendee attendee in allAttendees)
            {
                if (attendee.actuallyShowedUp == true) // On ne prend en compte les préférences que de celleux qui viennent effectivement lors de l'événement
                {
                    bool theirPreferedThemeAlreadyExists = false;
                    foreach (Theme theme in allPreferedThemes)
                    {
                        if (theme.name == attendee.preferedThemes[0].ToString())
                        {
                            theirPreferedThemeAlreadyExists = true;
                            // Console.WriteLine("Débug : son thème existe déjà ; je l'incrémente dans la liste des thèmes");
                            theme.occurrenceNumber += 1;
                        }
                    }
                    if (theirPreferedThemeAlreadyExists == false && attendee.preferedThemes[0].ToString() != "")
                    {
                        // Console.WriteLine("Débug : son thème n'existe pas ; je le crée et l'ajoute à la liste");
                        Theme instanceDeTheme = new Theme();
                        instanceDeTheme.name = attendee.preferedThemes[0].ToString();
                        instanceDeTheme.occurrenceNumber = 1;
                        allPreferedThemes.Add(instanceDeTheme);
                    }
                    attendeeDealtWith += 1;
                    // Console.WriteLine("Débug : j'ai traité {0} personnes sur {1} qu'il y a au total dans la liste", personnesTraitées, toustesLesTravailleureusesEGO.Count);

                    // Debug
                    //foreach (Theme theme in tousLesThemesPreferesChoix1)
                    //{
                    //    Console.WriteLine(theme.nom.ToString());
                    //}
                }
            }
            // Débug
            //foreach (Theme theme in tousLesThemesPreferesChoix1)
            //{
            //    Console.WriteLine(theme.nom.ToString());
            //}
        }

        public void SortThemes()
        {


            List<int> allOccurrences = new List<int>();
            foreach (Theme theme in allPreferedThemes)
            {
                allOccurrences.Add(theme.occurrenceNumber);
                // Console.WriteLine("Le thème {0} a {1} occurrences.", theme.nom, theme.occurrence); // Débug
            }
            allOccurrences.Sort();
            // Debug
            //Console.WriteLine("Les occurrences triées donnent : ");
            //foreach (int occurrences in toutesLesOccurrences)
            //{
            //    Console.WriteLine(occurrences);
            //}

            allOccurrences.Reverse();
            // Débug
            // Console.WriteLine("Les occurrences inversées donnent : ");
            //foreach (int occurrences in toutesLesOccurrences)
            //{
            //    Console.WriteLine(occurrences);
            //}

            foreach (Theme t in allPreferedThemes)
            {
                if (allOccurrences.Count >= 2 && t.occurrenceNumber >= allOccurrences[1])
                {
                    t.isMostPrefered = true;
                    // Debug
                    //Console.WriteLine(t.nom + " est le thème le plus aimé.");
                }
            }

            // Débug
            //foreach (Theme theme in tousLesThemesPreferesChoix1)
            //{
            //    if (theme.isMostPrefered)
            //    {
            //        Console.WriteLine(theme.nom + " est le theme préféré");
            //    }
            //    if (theme.isLeastPrefered)
            //    {
            //        Console.WriteLine(theme.nom + " est le theme le moins aimé");
            //    }
            //}

        }

        // III - Pour créer les ateliers
        public List<Workshop> allWorkshops = new List<Workshop>();
        public void SetUpWorkshops()
        {
            foreach (Theme theme in allPreferedThemes)
            {
                if (theme.isMostPrefered == true)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        SetUpWorkshop(theme);
                    }
                }
                else
                {
                    SetUpWorkshop(theme);
                }
            }
        }

        public void SetUpWorkshop(Theme theme)
        {
            Workshop newWorkshop = new Workshop(); // L'identifiant de la nouvelle instance importe peu puis durant runtime on ne va pas l'utiliser nous pour y accéder.

            newWorkshop.theme = theme;

            newWorkshop.number = allWorkshops.Count + 1;

            allWorkshops.Add(newWorkshop);
        }

        static Random randGenerator = new Random();

        public void FillInWorkshops()
        {
            // D'abord, il faut entre 0 et 1 animateur.rice dans chaque atelier.
            // Prio à celleux qui ont exprimé une préférence de thème et dont le thème préféré existe.
            foreach (Workshop workshop in allWorkshops)
            {
                if (workshop.members.Count < 1) // Là c'est juste pour gagner du temps d'exécution
                {
                    foreach (Attendee attendee in allAttendees)
                    {
                        if (attendee.animator == true && attendee.assignedWorkshop == null && attendee.actuallyShowedUp == true && workshop.theme.ToString() == attendee.preferedThemes[0].ToString() && workshop.members.Count < 1) // Là c'est parce que sinon dans un même atelier qui était à 0 avant on peut ajouter deux personnes à la suite avant de revérifier si c'est toujours à 0, donc need vérif à chque travailleureuse
                        {
                            workshop.members.Add(attendee);
                            attendee.assignedWorkshop = workshop;
                        }
                    }
                }
            }
            // Puis celleux du GT qui n'ont pas exprimé de choix ou dont le thème préféré n'existe pas.
            foreach (Workshop workshop in allWorkshops)
            {
                if (workshop.members.Count < 1) // Là c'est juste pour gagner du temps d'exécution
                {
                    foreach (Attendee attendee in allAttendees)
                    {
                        if (attendee.animator == true && attendee.assignedWorkshop == null && attendee.actuallyShowedUp == true && workshop.members.Count < 1) // Là c'est parce que sinon dans un même atelier qui était à 0 avant on peut ajouter deux personnes à la suite avant de revérifier si c'est toujours à 0, donc need vérif à chque travailleureuse)
                        {
                            workshop.members.Add(attendee);
                            attendee.assignedWorkshop = workshop;
                        }
                    }
                }
            }

            // Verbose
            // Vérification concernant celleux du GT
            foreach (Attendee attendee in allAttendees)
            {
                if (attendee.animator == true && attendee.assignedWorkshop == null && attendee.actuallyShowedUp == true)
                {
                    Console.WriteLine("Mince ! {0} fait partie du GT et n'a pas été assigné à un atelier !", attendee.LastNames);
                }
            }
            foreach (Workshop workshop in allWorkshops)
            {
                if (workshop.members.Count > 1)
                {
                    Console.WriteLine("Mince ! {0} a déjà plus de un.e member alors qu'on ne devrait avoir assigné que 0 ou 1 member du GT par atelier !", workshop.theme.name + workshop.number.ToString());
                }
            }
            // Idéalement on pourrait ajouter une vérif que deux personnes du GT ne sont pas dans le même atelier, mais je ne l'ai pas encore fait.

            // Parmi les personnes qui ne font pas partie du GT, priorité à celleux qui ont exprimé leurs préférences et dont l'atelier privilégié existe
            foreach (Workshop workshop in allWorkshops)
            {
                foreach (Attendee attendee in allAttendees)
                {
                    if (attendee.assignedWorkshop == null && attendee.actuallyShowedUp == true && workshop.members.Count < 8 && workshop.theme.ToString() == attendee.preferedThemes[0].ToString())
                    {
                        workshop.members.Add(attendee);
                        attendee.assignedWorkshop = workshop;
                    }
                    // Verbose
                    // Vérification
                    if (attendee.assignedWorkshop == null && attendee.actuallyShowedUp == true && workshop.members.Count > 8 && workshop.theme.ToString() == attendee.preferedThemes[0].ToString())
                    {
                        Console.WriteLine("Mince ! {0} ne peut pas aller dans l'atelier de son premier choix alors qu'il existe, parce qu'il est plein.", attendee.LastNames);
                        Console.WriteLine();
                    }
                }
            }

            // Ensuite celleux qui ont exprimé leurs préférences mais dont l'atelier privilégié n'existe pas ; peut-être le second choix...
            foreach (Workshop workshop in allWorkshops)
            {
                foreach (Attendee attendee in allAttendees)
                {
                    if (attendee.assignedWorkshop == null && attendee.actuallyShowedUp == true && workshop.members.Count < 8 && workshop.theme.ToString() == attendee.preferedThemes[1].ToString())
                    {
                        workshop.members.Add(attendee);
                        attendee.assignedWorkshop = workshop;
                    }
                    // Verbose
                    // Vérification
                    if (attendee.assignedWorkshop == null && attendee.actuallyShowedUp == true && workshop.members.Count > 8 && workshop.theme.ToString() == attendee.preferedThemes[0].ToString())
                    {
                        Console.WriteLine("Mince ! {0} ne peut pas aller dans l'atelier de son deuxième choix alors qu'il existe, parce qu'il est plein.", attendee.LastNames);
                        Console.WriteLine();
                    }
                }
            }
            // Ou le troisième choix ?
            foreach (Workshop workshop in allWorkshops)
            {
                foreach (Attendee attendee in allAttendees)
                {
                    if (attendee.assignedWorkshop == null && attendee.actuallyShowedUp == true && workshop.members.Count < 8 && workshop.theme.ToString() == attendee.preferedThemes[2].ToString())
                    {
                        workshop.members.Add(attendee);
                        attendee.assignedWorkshop = workshop;
                    }
                    // Verbose
                    // Vérification
                    if (attendee.assignedWorkshop == null && attendee.actuallyShowedUp == true && workshop.members.Count > 8 && workshop.theme.ToString() == attendee.preferedThemes[0].ToString())
                    {
                        Console.WriteLine("Mince ! {0} ne peut pas aller dans l'atelier de son troisième choix alors qu'il existe, parce qu'il est plein.", attendee.LastNames);
                        Console.WriteLine();
                    }
                }
            }

            // Ensuite les autres sont réparties aléatoirement
            // Pour faire ça, on commence par composer une liste des personnes restantes
            List<Attendee> orderedUnassigned = new List<Attendee>();

            foreach (Attendee attendee in allAttendees)
            {
                if (attendee.actuallyShowedUp == true && attendee.assignedWorkshop == null)
                {
                    orderedUnassigned.Add(attendee);
                }
            }

            // Puis on la shuffle
            List<Attendee> shuffledUnassigned = new List<Attendee>();
            shuffledUnassigned = ShuffleList(orderedUnassigned);

            // Ensuite on assigne les personnes de cette nouvelle liste dans les ateliers, dans l'ordre
            foreach (Workshop workshop in allWorkshops)
            {
                while (workshop.members.Count < 8)
                {
                    if (shuffledUnassigned.Count > 0)
                    {
                        workshop.members.Add(shuffledUnassigned[0]);
                        shuffledUnassigned[0].assignedWorkshop = workshop;
                        shuffledUnassigned.RemoveAt(0);
                    }
                    else
                    {
                        break;
                    }
                }
            }
            // Verbose
            // Vérification pour toutes les personnes présentes
            foreach (Attendee attendee in allAttendees)
            {
                if (attendee.assignedWorkshop == null && attendee.actuallyShowedUp == true)
                {
                    Console.WriteLine("Mince ! {0} n'est dans aucun atelier.", attendee.LastNames);
                    Console.WriteLine();
                }
            }
        }

        // Cette fonction a été prise directement sur Internet sauf que j'ai utilisé mon propre générateur random vu que j'en ai déjà un et j'ai mis mes propres types de liste (List<Attendee>).
        public List<Attendee> ShuffleList(List<Attendee> list)
        {

            // public static void Shuffle<T>(this IList<T> list)
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = randGenerator.Next(n + 1);
                Attendee value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
            return list;
        }

        public void ListWorkshopsAndMembers()
        {
            Console.WriteLine("Résultats de la constitution des ateliers : \n");
            foreach (Workshop workshop in allWorkshops)
            {
                if (workshop.members.Count > 0)
                {
                    Console.WriteLine("L'atelier numéro {0} porte sur le thème \"{1}\" et a comme members : \n", workshop.number, workshop.theme.name);

                    foreach (Attendee member in workshop.members)
                    {
                        Console.WriteLine(member.LastNames);
                    }
                    Console.WriteLine(); // Pour sauter une ligne uniquement à la fin.
                }
                
            }
        }

        public void OutputCSV()
        {

            List<string> outputCSVList = new List<string>();
            string header = "0 Prénoms des personnes,1 Noms de famille,2 Numéro de l'atelier, 3 Thème de l'atelier";

            outputCSVList.Add(header);

            foreach (Attendee attendee in allAttendees)
            {
                string oneLineString = "";

                oneLineString += attendee.FirstNames + ",";
                oneLineString += attendee.LastNames + ",";

                if (attendee.assignedWorkshop != null)
                {
                    oneLineString += attendee.assignedWorkshop.number + ",";
                    oneLineString += attendee.assignedWorkshop.theme.name;
                }
                else
                {
                    if (attendee.actuallyShowedUp == true)
                    {
                        oneLineString += "Présent.e mais pas dans un atelier" + "," + " ";
                    }
                    else
                    {
                        oneLineString += "Pas présent.e donc pas dans un atelier" + "," + " ";
                    }
                }


                outputCSVList.Add(oneLineString);
            }

            using (StreamWriter outCsv = new StreamWriter(@"output.csv"))
            {
                foreach (string line in outputCSVList)
                {
                    outCsv.WriteLine(line);
                }
            }
        }
    }
}
